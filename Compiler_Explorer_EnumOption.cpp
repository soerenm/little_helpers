#include <iostream>
#include <type_traits>
#include <utility>

enum class Option {
    A = 1,
    B = 2,
    C = 4,
};

template <typename EnumType,
          typename = std::enable_if_t<
              std::is_enum<EnumType>::value &&
              !std::is_convertible<EnumType,
                                   std::underlying_type_t<EnumType>>::value>>
// C++ 23 alternative: std::is_scoped_enum_v<EnumType>>
class Options {
    using bitfield_t = std::underlying_type_t<EnumType>;
    bitfield_t bitfield{};

   public:
    Options() = default;

    explicit Options(EnumType opt) {
        this->bitfield |= static_cast<bitfield_t>(opt);
    }

    void setOption(EnumType opt) {
        this->bitfield |= static_cast<bitfield_t>(opt);
    }

    // C++ 11 - Version
    template <typename... Args>
    void setOptions(Args&&... args) {
        //  https://www.fluentcpp.com/2019/03/05/for_each_arg-applying-a-function-to-each-argument-of-a-function-in-cpp/
        std::initializer_list<int>{
            ((void)this->setOption(std::forward<Args>(args)), 0)...};
    }

    // C++ 17 - Version
    // https://www.fluentcpp.com/2021/06/07/how-to-define-a-variadic-number-of-arguments-of-the-same-type-part-5/
    template <typename... Args, typename = std::enable_if_t<std::conjunction_v<
                                    std::is_same<EnumType, Args>...>>>
    void setOptions2(Args&&... args) {
        // static_assert(std::conjunction_v<std::is_same<Args, EnumType>...>,
        //               "All parameters must be of same template type
        //               EnumType");
        //    https://www.fluentcpp.com/2019/03/05/for_each_arg-applying-a-function-to-each-argument-of-a-function-in-cpp/
        (this->setOption(std::forward<Args>(args)), ...);
    }

    void resetOption(EnumType opt) {
        this->bitfield &= ~static_cast<bitfield_t>(opt);
    }

    bool isOptionSet(EnumType opt) {
        return this->bitfield & static_cast<bitfield_t>(opt);
    }

    void reset() { this->bitfield = bitfield_t{}; }

    bitfield_t getBitfield() { return this->bitfield; }
};

int main() {
    Options<Option> opt = Options<Option>();
    opt.setOption(Option::A);
    opt.setOptions(Option::B, Option::C);
    opt.setOptions2(Option::B, Option::C);
    opt.resetOption(Option::C);
    return opt.getBitfield();
}